import  axios  from "axios";

const getPage = async (numPage) => {
   const response = await axios.get(`https://reqres.in/api/users?page=${numPage}`,{
       headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    })
   return response
}

export default getPage