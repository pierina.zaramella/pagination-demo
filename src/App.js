import React, {Component} from 'react';
import styles from './App.css';
import  getPage  from './App.service'

let users;
let pagination;
export default class App extends Component {
  
  state = {
    users: null,
    total: null,
    per_page: null,
    current_page: null
  }

  componentDidMount(){
    this.makeHttpRequestWithPage(1)
  }

  makeHttpRequestWithPage = async pageNumber => {
    let {data:response} = await getPage(pageNumber)
    console.log('response ', response)
    const {data} = response;
    console.log('response ', data)

    this.setState({
      users: data,
      total: response.total,
      per_page: response.per_page,
      current_page: response.page,
    })
  }

  getPagination(){
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(this.state.total / this.state.per_page); i++) {
        pageNumbers.push(i);
    }
    pagination = pageNumbers.map((i) => {
    let classes = this.state.current_page === i ? styles.active : '';
    return (<span key={i} className={classes} onClick={() => this.makeHttpRequestWithPage(i)}>{i}</span>)
  }) 
    console.log('pagination ', pagination)

  }

  render() {
    if(this.state.users){
      users = this.state.users.map((user) => (
        <tr key={user.id}>
          <td>{user.id}</td>
          <td>{user.first_name}</td>
          <td>{user.last_name}</td>
        </tr>
      ))

      this.getPagination()

      console.log(users)
    }
    
    return (

      <div className="app">
        <table>
          <thead>
            <tr>
              <th>S/N</th>
              <th>First Name</th>
              <th>Last Name</th>
            </tr>
          </thead>
          <tbody>{users}
          </tbody>
        </table>
        <div className="pagination">
          <span onClick={() => this.makeHttpRequestWithPage(1)}>&laquo;</span>
          { pagination }
          <span onClick={() => this.makeHttpRequestWithPage(1)}>&raquo;</span>
        </div>
      </div>
    );
  }
}
